<!-- SESSION -->
<?php session_start();?>


<!-- SCRIPTS -->
<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/login-dialog.js"></script>
<script src="js/functions.js"></script>

<!-- FONTS -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<!-- META DATA -->
<meta charset="UTF-8">

<!-- STYLESHEETS -->
<link rel="stylesheet" type="text/css" href="styles/reset.css">
<link rel="stylesheet" type="text/css" href="styles/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="styles/main.css">

<!-- DATABASE CONNECTION -->
<?php require('includes/connection.php');?>

<?php require('includes/functions.php');?>

<!-- SESSION CHECK -->

<?php
    
    if(isset($_SESSION['id'])) {
        if ($_SESSION['ip'] != $_SERVER['REMOTE_ADDR']) {
           header('Location: posts/logout.php'); 
        }
    }
?>

<!-- CLASSES -->
<?php include("classes/dish.php"); ?>
<?php include("classes/rijsttafel.php"); ?>

<!-- TITLE -->
<?php $title="Yuen's Garden | ";?>
