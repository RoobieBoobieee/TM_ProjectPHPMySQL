
<?php
    if (isset($_SESSION['ip']) && $_SERVER['REMOTE_ADDR'] != $_SESSION['ip'])
    {
        header('Location: posts/logout.php');
    }
?>

<nav class="ui-widget-content">
<ul>
<!--   Guest Section -->
    <li><a href="index.php">Menu</a></li>
    <li><a href="rijsttafels.php">Rijsttafels</a></li>
    <li><a href="contact.php">Contact</a></li>
    
<!--   User Section -->
    <?php 
    if (isset($_SESSION['level']) && ($_SESSION['level'] == 1 || $_SESSION['level'] == 2)) {
        echo(" || ");
        echo("<li><a href=\"tafelverdeling.php\">Tafelverdeling</a></li>");    
        echo("<li><a href=\"reserveren.php\">Reserveren</a></li>");
    } ?>
<!--   Admin section  -->
    <?php 
    if (isset($_SESSION['level']) && $_SESSION['level'] == 2) {
        echo(" || ");
        /*echo("<li><a href=\"#\">Info</a></li>");*/
        echo("<li><a href=\"adminmenu.php\">Menu</a></li>");
        echo("<li><a href=\"adminusers.php\">Gebruikers</a></li>");
        echo("<li><a href=\"adminrt.php\">Rijsttafels</a></li>");
        echo("<li><a href=\"adminres.php\">Reserveringen</a></li>");
    } ?>
</ul>
    
<!--   Log in -->
    
<?php 

if(isset($_SESSION['name'])){
        echo ("<div id=\"logOut\">Welkom, <a href=\"edituser.php?id=" .  $_SESSION['id'] . "\">" . $_SESSION['name'] . "</a> <br /> 
            <a href=\"posts/logout.php\">Uitloggen</a></div>");
      } else {
        echo ("<button id=\"logIn\">Inloggen</button>");
      }
?>
    
</nav>

<!-- Login form (enkel bereikbaar via login knop) -->
<div id="login-form" title="Inloggen"> 
  <form name="frmlogin" id="frmlogin" action="posts/login.php" onsubmit="return validateLogin()" method="post">
    <fieldset>
        <p class="errorTips"></p>
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="text ui-widget-content ui-corner-all">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="text ui-widget-content ui-corner-all">
        Nog geen account? Registreer <a href="register.php">hier</a>!
        <!-- Allow form submission with keyboard without duplicating the dialog button -->
        <input type="submit" name="btnlogin" id="btnlogin" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>

