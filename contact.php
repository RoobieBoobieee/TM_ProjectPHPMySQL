<!DOCTYPE html>
<html>
<head>
    
<?php require('includes/header.php');?>

    
<title><?php echo($title);?>Contact</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu">
    <?php 
        if(isset($_GET["err"]) && $_GET["err"] == 1)
        {
            echo ("<div class=\"ui-state-error ui-corner-all\"><p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><strong>Fout:</strong> E-mail/Wachtwoord Combinatie Bestaat Niet</p></div>");
        }
    ?>

    <?php
        
        echo ("<div class=\"text ui-widget-content menucategory front\">
        
        <h2>Chinees Restaurant</h2>
        
        <h1>Yuen's Garden</h1>
        
        
        <h3>Meeneem Prijslijst</h3>
        
        <h3>Prijzen BTW inbegrepen</h3>
        
        
        <h2>Telefoon:</h2>
        <h2>(03) 899 10 01</h2>
        
        
        <h3>Dinsdag gesloten<br />
        (behalve op feestdagen)<br />
        Maandag - zaterdag open vanaf 18.00 uur<br />
        Zondag open vanaf 12.00 tot 14.00 uur<br />
        18.00 tot 21.00 uur</h3>
        
        
        <h2>Guido Gezellenlaan 81<br />
        2870 Puurs<br />
        Belgie</h2>

        
        
        </div>");
    ?>

</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>