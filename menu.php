<div id="column1"></div>
<div id="column2"></div>
<div id="column3"></div>

<?php
require('includes/connection.php');

include("classes/dish.php");

$search = "";

if(isset($_POST['search']) && (!empty($_POST['search'])))
{
    $search=mysqli_real_escape_string($conn,htmlspecialchars($_POST['search']));

}
echo ("<div class=\"text ui-widget-content menucategory front\">
       
        <h2>Chinees Restaurant</h2>
        
        <h1>Yuen's Garden</h1>
        
        
        <h3>Meeneem Prijslijst</h3>
        
        <h3>Prijzen BTW inbegrepen</h3>
        
        
        <h2>Telefoon:</h2>
        <h2>(03) 899 10 01</h2>
        
        
        <h3>Dinsdag gesloten<br />
        (behalve op feestdagen)<br />
        Maandag - zaterdag open vanaf 18.00 uur<br />
        Zondag open vanaf 12.00 tot 14.00 uur<br />
        18.00 tot 21.00 uur</h3>
        
        
        <h2>Guido Gezellenlaan 81<br />
        2870 Puurs<br />
        Belgie</h2>

        
        
        </div>");
        
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM tblmenu WHERE `name` LIKE '%" . $search . "%';";

        
        // query uitvoeren
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
        // elke rij afgaan en afdrukken
        if ($result->num_rows > 0) {
            
            //nieuwe dish
            $dish = new Dish();
            
            // output data of each row
            while($row = $result->fetch_assoc()) {
               
               
               
               // headers van tabellen fixen - nieuwe categorie = nieuwe tabel
               if (strcmp($row["category"], $dish->Categorie) !== 0) {
                   
                   // als dishnummer leeg is is dit da aller eerste loop en dus geen tabel om af te sluiten
                   if(isset($dish->Nummer)){
                       echo("</tbody></table>");
                   }
                   
                   /* &#21488; = Taipei of Taibei [taĭ'peː'] is sinds het einde van de Chinese Burgeroorlog in 1949 de facto 
                   de hoofdstad van de Republiek China en was tot 1956 tevens de hoofdstad van de provincie Taiwan. */
                   echo("<table class=\"text ui-widget-content menucategory\" name=\"" . $row["category"] . "\"><caption>&#21488; - " . $row["category"] . "</caption>");
                   
                   // standaard header printen NUMMER | GERECHT | PRIJS
                   echo("<thead><tr><th>#</th><th class=\"fullwidth\">Gerecht</th><th class=\"alignright\">Prijs</th></tr></thead><tbody>");
                   
               }
               
               $dish->Nummer = $row["number"];
	           $dish->Naam =  $row["name"];
	           $dish->Prijs = $row["price"];
	           $dish->Beschrijving = $row["description"];
               $dish->Categorie = $row["category"];
               
               echo "<tr>" . $dish . "</tr>";

            }
            echo("</tbody></table>");
        }
?>