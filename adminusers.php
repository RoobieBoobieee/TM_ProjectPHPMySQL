<!DOCTYPE html>
<html>
<head>    
<?php require('includes/header.php');?>

<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
 ?>     
<title><?php echo($title);?>Gebruikers</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 

<?php
        if (isset($_GET["remid"]))
        {
            //User met ID verwijderen
            $query = "DELETE FROM `tblusers` WHERE `id` = " . mysqli_real_escape_string($conn,htmlspecialchars($_GET["remid"])) . ";";
        
            // query uitvoeren
            $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        }
    
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM tblusers WHERE '1';";
        
        // query uitvoeren
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
        // elke rij afgaan en afdrukken
        if ($result->num_rows > 0) {
            
          echo ("<table id=\"adminuserstable\" class=\"text ui-widget-content ui-corner-all fullwidth\">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>E-mail</th>
                        <th>Voornaam</th>
                        <th>Achternaam</th>
                        <th>Straat</th>
                        <th>Huis#</th>
                        <th>Post#</th>
                        <th>Stad</th>
                        <th>Land</th>
                        <th>Admin?</th>
                    </tr>
                </thead>
                <tbody>");
            
           // output data of each row
	       while($row = $result->fetch_assoc()) {
               
               if ($row["level"] == 2) {
                   $admin = "Ja";
               } else {
                   $admin = "Nee";
               }
               
               echo ("<tr>
                        <td class=\"alignright\">" . $row["id"] . "</td>
                        <td>" . $row["email"] . "</td>
                        <td>" . $row["firstname"] . "</td>
                        <td>" . $row["lastname"] . "</td>
                        <td>" . $row["street"] . "</td>
                        <td class=\"alignright\">" . $row["number"] . "</td>
                        <td class=\"alignright\">" . $row["postal"] . "</td>
                        <td>" . $row["city"] . "</td>
                        <td>" . $row["country"] . "</td>
                        <td class=\"alignright\">" . $admin . "</td>
                        <td>" . "<button class=\"btnedituser\" id=\"" . $row["id"] . "\">Aanpassen</button>" . "</td>
                        <td>" . "<button class=\"btnremuser\" id=\"" . $row["id"] . "\">Verwijderen</button>" . "</td>
                        
                    </tr>");
           }
            
           echo("</tbody></table>");
            
        }
?>
  
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>       