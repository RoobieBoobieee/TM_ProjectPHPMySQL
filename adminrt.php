<!DOCTYPE html>
<html>
<head>
    
<?php require('includes/header.php');?>

    
<title><?php echo($title);?>Rijsttafels</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 
        <div id="column1"></div>
        <div id="column2"></div>
        <div id="column3"></div> 
        
    <?php
        
           
     
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM `tblrijsttafel` WHERE '1' ORDER BY `npeople`;";
        
        // query uitvoeren
        $rijsttafel = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
    
        $rijsttafels = array();
        
        // elke rij afgaan en afdrukken
        if ($rijsttafel->num_rows > 0) {
	       while($rtrow = $rijsttafel->fetch_assoc()) {
               
               $reference = false;
               $npeople = $rtrow["npeople"];

               foreach ($rijsttafels as $rt) {
                    if ($rt->Personen == $npeople) 
                    {
                        $rtobj = &$rt;
                        $reference = true;
                        break; 
                    }
                }
               
               if (!$reference)
               {
                   $rtobj = new Rijsttafel;
                   $rtobj->Personen = $npeople;
                   array_push($rijsttafels, $rtobj);
                   $len = count($rijsttafel) - 1;
                   $rtobj = &$rijsttafels[key($rijsttafels)];
                   
               }
                       
                $query = "SELECT * FROM `tblmenu` WHERE `id` = " . mysqli_real_escape_string($conn,htmlspecialchars($rtrow["dishid"]));
               
               $menu = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
               
    
               if ($menu->num_rows > 0) {
                        while($menurow = $menu->fetch_assoc()) {
                               
                       if (strcmp($menurow["category"], "Soepen") == 0)
                       {
                           $rtobj->SoepToevoegen($menurow["name"]);
                       } elseif  (strcmp($menurow["category"], "Voorgerechten") == 0) {
                           $rtobj->Voorgerecht = $menurow["name"];
                       } else {
                           $rtobj->GerechtToevoegen($menurow["name"]);
                       }                
                  }
              }
           }
        }
    foreach ($rijsttafels as $rt) 
    {
        $query = "SELECT `price` FROM `tblrijsttafelprices` WHERE `npeople` = " . $rt->Personen;
               
        $menu = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
               
        if ($menu->num_rows > 0) {
	       $menurow = $menu->fetch_assoc(); 
           $rt->Prijs = $menurow["price"];
        }
         echo($rt);
        echo("</tr><tr><td>" . "<button class=\"btneditrt\" id=\"" .  $rtobj->Personen . "\">Aanpassen</button>" . "</td>");
        echo("</tr></tbody></table>");
                
    }
        
    echo ("<script type=\"text/javascript\">SpreadTables(\"#column\", 3, \".menucategory\");</script>");
    ?>
    
 </div>   
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>