<!DOCTYPE html>
<html>
<head>
<?php require('includes/header.php');?>
<title></title>

</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 

    <h1><?php echo($title);?>Registreren</h1>
        
    <?php 
        if(isset($_GET["err"]) && $_GET["err"] == 1){
            echo ("<div class=\"ui-state-error ui-corner-all\"><p><span class=\"ui-icon ui-icon-alert\" style=\"float: left; margin-right: .3em;\"></span><strong>Fout:</strong> E-mail adres is al geregistreed</p></div>");
        }
    
    ?>
        <div class="errorTips"></div>

    
    
    <form name="frmregistration" action="posts/registration.php" onsubmit="return validateRegistration()" method="post">
        <div id="column1"></div>
        <div id="column2"></div>
        <div id="column3"></div> 
    <fieldset class="regfieldset ">
        <legend>Account</legend>
        <label for="name">Voornaam:</label>
        <input type="text" name="name" id="name" class="ui-widget-content ui-corner-all" maxlength="32" required>
        <label for="lastname">Achternaam:</label>
        <input type="text" name="lastname" id="lastname" class="ui-widget-content ui-corner-all" maxlength="32" required>
        <label for="email">E-mail:</label>
        <input type="text" name="email" id="email" class="ui-widget-content ui-corner-all" maxlength="64" required>
        <label for="password">Wachtwoord:</label>
        <input type="password" name="password" id="password" class="ui-widget-content ui-corner-all" required>
        <label for="REpassword">Herhaal wachtwoord:</label>
        <input type="password" name="REpassword" id="REpassword" class="ui-widget-content ui-corner-all" required>
    </fieldset>
        
    <fieldset class="regfieldset">
        <legend>Locatie</legend>
        <label for="street">Straat:</label>
        <input type="text" name="street" id="street" class="ui-widget-content ui-corner-all" maxlength="128" required>
        <label for="housenum">Nummer:</label>
        <input type="number" name="housenum" id="housenum" class="ui-widget-content ui-corner-all" maxlength="4" required>
        <label for="postal">Postcode:</label>
        <input type="postal" name="postal" id="postal" class="ui-widget-content ui-corner-all" maxlength="32" required>
        <label for="city">Stad:</label>
        <input type="city" name="city" id="city" class="ui-widget-content ui-corner-all" maxlength="8" required>
        <label for="country">Land:</label>
        <input type="text" name="country" id="country" class="ui-widget-content ui-corner-all" maxlength="32" required>
     </fieldset>
      <fieldset class="regfieldset">  
     <input type="submit" class="ui-widget-content ui-corner-all">
        </fieldset>
    <?php echo ("<script type=\"text/javascript\">SpreadTables(\"#column\", 3, \".regfieldset\");</script>"); ?> 
    </form>
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>