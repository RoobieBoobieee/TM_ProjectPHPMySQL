<!DOCTYPE html>
<html>
<head>
<?php require('includes/header.php');?>
<script type="text/javascript">
<!--
	$(document).ready(function()
	{
		$("#search").keyup(function()
		{
			$zoekterm = $("#search").val();
						
			$request = $.ajax({
				method:"POST",
				url:"menu.php",
				data: {search: $zoekterm},
			});
			
			$request.done(function(msg)
			{
                $("main").empty();
				$("main").append(msg);
                FixTables("#column", 3, ".menucategory");
			});
			
			$request.fail(function(jqXHR, textStatus)
			{
				$("main").html("Request failed: " + textStatus);
			});
			
		});
      
        $('#search').keyup();
	
	});
//-->
</script>
<title><?php echo($title);?></title>
</head>
<body>
    
<?php require('includes/nav.php');?>

<form action="#" id="frmsearch" method="post">
	  <p>
		Zoeken:<input type="text" id="search" name="search" class="ui-widget-content"/>
	  </p>
</form>
<main class="menu"> 
   
	
	 
</main>
<script type="text/javascript">FixTables("#column", 3, ".menucategory");</script>
    
<?php require('includes/footer.php');?>
</body>  
</html>
