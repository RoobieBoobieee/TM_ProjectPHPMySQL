<!DOCTYPE html>
<html>
<head>
    
<?php require('includes/header.php');?>


    
<title><?php echo($title);?>Home</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu">
 <div class="ui-widget clear">
     <?php
     if (isset($_GET["err"])) {
     echo ('<div class="ui-state-highlight" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
		<strong>Opgelet!</strong> Reservering mislukt!</p></div>');
     }
     ?>
	<div class="ui-state-highlight" style="margin-top: 20px; padding: 0 .7em;">
		<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
		<strong>Opgelet!</strong> Gelieve 1 dag op voorhand te reserveren.</p>
    </div>
</div>
    


<p>Datum: <input type="text" id="datepicker"></p> 

    <div id="vrij"></div>
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>