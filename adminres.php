<!DOCTYPE html>
<html>
<head>    
<?php require('includes/header.php');?>

<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
 ?>     
<title><?php echo($title);?>Reservaties</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 

<?php
        
    
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM tblreservations WHERE '1';";
        
        // query uitvoeren
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
        // elke rij afgaan en afdrukken
        if ($result->num_rows > 0) {
            
          echo ("<table id=\"adminuserstable\" class=\"text ui-widget-content ui-corner-all fullwidth\">
                <thead>
                    <tr>
                        <th>Gebruiker</th>
                        <th>Datum</th>
                        <th>Tafel</th>
                    </tr>
                </thead>
                <tbody>");
            
           // output data of each row
	       while($row = $result->fetch_assoc()) {
               
                // selecteer al de items in de menu tabel
                $query = "SELECT `email` FROM tblusers WHERE id = " . $row["userid"] . ";";
        
                // query uitvoeren
               
                $result2 = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
                // elke rij afgaan en afdrukken
                if ($result2->num_rows > 0) {
                    
                    while($row2 = $result2->fetch_assoc()) {
                
                        echo ("<tr>
                                 <td class=\"alignright\">" . $row2["email"] . "</td>
                                 <td>" . $row["date"] . "</td>
                                 <td>" . $row["tblnum"] . "</td>
                                             
                            </tr>");
                    }
                }
           }
            
           echo("</tbody></table>");
            
        }
?>
  
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>       