<?php
session_start();
require('../includes/connection.php');
$date = mysqli_real_escape_string($conn,htmlspecialchars($_POST["date"]));
$date = date('Y-m-d', strtotime(str_replace('-', '/', $date)));   
$query = "SELECT * FROM `tblreservations` WHERE `tblnum` = '" . mysqli_real_escape_string($conn,htmlspecialchars($_POST["tblnum"])) . "' AND  `date` = '" . $date . "'";

$result = mysqli_query($conn, $query);

if ($result->num_rows > 0) {   
    header('Location: ../reserveren.php?err=1');
   exit();
} else {

$query = "INSERT INTO `tblreservations` (`id`, `tblnum`, `date`, `userid`) VALUES (NULL, '" . mysqli_real_escape_string($conn,htmlspecialchars($_POST["tblnum"])) . "', '" . mysqli_real_escape_string($conn,htmlspecialchars($date)) . "', '" . mysqli_real_escape_string($conn,htmlspecialchars($_SESSION['id'])) . "')";

mysqli_query($conn, $query);

}

   header('Location: ../index.php');
   exit();

?>