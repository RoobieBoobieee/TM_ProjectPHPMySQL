<?php

session_start();

require('../includes/connection.php');

$email = mysqli_real_escape_string($conn,htmlspecialchars($_POST["email"]));
$email = strtoupper($email);
$pw = mysqli_real_escape_string($conn,htmlspecialchars($_POST["password"]));
$pw = md5($pw);

$query = "SELECT `id`, `firstname`, `level` FROM `tblusers` WHERE `email` = '$email' AND `password` = '$pw'";

$result = mysqli_query($conn, $query);

// Als er een correcte mail-wachtwoord combinatie is gevonden
if ($result->num_rows > 0) {

    $row = $result->fetch_assoc();
    
    $_SESSION['id'] = $row["id"];
    $_SESSION['name'] = $row["firstname"];
    $_SESSION['level'] = $row["level"];
    $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
    header('Location: ../index.php');
    exit();
}

header('Location: ../index.php?err=1');

exit();
?>