<?php
	//start the session
	session_start();
	
	//check to make sure the session variable is registered
	if(isset($_SESSION['name']))
	{
		//session variable is registered, user ready to logout
		session_unset();
		session_destroy();
	}
	
    header('Location: ../index.php');
    exit();
?>
