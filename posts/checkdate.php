<?php
require('../includes/connection.php');

$tables = array();
for ($i = 1; $i <= 16; $i++) {
    if ($i == 2 || $i == 15) {
        $tables[$i] = false;
    } else {
        $tables[$i] = true;
    }
}

$date = "";

if(isset($_POST['date']) && (!empty($_POST['date'])))
{
    $date=mysqli_real_escape_string($conn,htmlspecialchars($_POST['date']));
    $date = date('Y-m-d', strtotime(str_replace('-', '/', $date)));   
}
        
// selecteer al de items in de menu tabel
$query = "SELECT `tblnum` FROM tblreservations WHERE `date` LIKE '" . $date . "';";

        
// query uitvoeren
$result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
// elke rij afgaan 
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $tables[$row["tblnum"]] = false;                
    }
}
echo ("<form id=\"frmres\" action=\"posts/reservation.php\" method=\"post\">");
echo ("Tafelnummer: <select name=\"tblnum\" id=\"tblnum\" >");
for ($i = 1; $i <= 16; $i++) {
    if ($tables[$i] == true) {
        echo ("<option value=\"" . $i . "\">" . $i . "</option>");
    }
}
echo ("</select>");
echo ("<input type=\"submit\" name=\"btnres\" id=\"btnres\" >");
echo ("<input type=\"hidden\" name=\"date\" id=\"date\" value=\"" . $date . "\">");
echo ("</form>");
?>