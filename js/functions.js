function updateTips(t) {
    $(".errorTips")
        .text(t)
        .addClass("ui-state-highlight");
    setTimeout(function () {
        $(".errorTips").removeClass("ui-state-highlight", 1500);
    }, 500);
}

function checkEmpty(o, n) {
    if (o.val().length === 0) {
        o.addClass("ui-state-error");
        updateTips(n + " mag niet leeg zijn.");
        return false;
    } else {
        return true;
    }
}

function validateRegistration() {
    var pw = document.getElementById("password").value;
    var REpw = document.getElementById("REpassword").value;

    if (pw == REpw) {
        return true;
    } else {
        updateTips("Wachtwoorden zijn niet gelijk.");
        return false;
    }
}


function validateLogin() {
    var valid = true;
    allFields.removeClass("ui-state-error");
    valid = valid && checkEmpty(email, "E-mail");
    valid = valid && checkEmpty(password, "Wachtwoord");
    if (valid) {
        return true;
    } else {
        return false;
    }
}

function FixTables($column, $columns, $object) {

    var totalheight = 0;
    var i = 1;
    var currentcol = $column + i;

    $($object).each(function () {
        totalheight += $(this).height();
    });

    var maxheight = totalheight / $columns;

    $($object).each(function () {

        $(this).appendTo(currentcol);

        if (($(currentcol).height() > maxheight) && (i < 3)) {
            i++;
            currentcol = $column + i;
        }
    });
}

function SpreadTables($column, $columns, $object) {

    var i = 1;
    var currentcol = $column + i;

    $($object).each(function () {

        $(this).appendTo(currentcol);
        i++;
        if (i == 4) {
            i = 1;
        }
        currentcol = $column + i;

    });
}

$(function () {
    $(".btneditmenu")
        .button()
        .click(function (event) {
            window.location = "editmenu.php?id=" + this.id;
        });
});

$(function () {
    $(".btnremuser")
        .button()
        .click(function (event) {
            window.location = "adminusers.php?remid=" + this.id;
        });
});


$(function () {
    $(".btnedituser")
        .button()
        .click(function (event) {
            window.location = "edituser.php?id=" + this.id;
        });
});

$(function () {
    $(".btneditrt")
        .button()
        .click(function (event) {
            window.location = "editrt.php?id=" + this.id;
        });
});

$(function () {
    $(".btnremdish")
        .button()
        .click(function (event) {
            window.location = "?" + this.id;
        });
});

$(function () {
    $(".btnaddrt")
        .button()
        .click(function (event) {
            window.location = "addrt.php?id=" + this.id;
        });
});

$(function () {
    var dateToday = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    $("#datepicker").datepicker({
        altFormat: 'dd-mm-yyyy',  // Date Format used
        firstDay: 1, // Start with Monday
        minDate: dateToday,
        maxDate: "+1Y",
        beforeShowDay: function (d) {
            var dy = d.getDay();
            if (dy == 2) {
                return [false, ""]
            } else {
                return [true, ""]
            }
        },
        onSelect: function() {
            $zoekterm = $("#datepicker").val();
			
            $request = $.ajax({
				method:"POST",
				url:"posts/checkdate.php",
				data: {date: $zoekterm},
			});
			
			$request.done(function(msg)
			{
                $("#vrij").html(msg);
			});
			
			$request.fail(function(jqXHR, textStatus)
			{
				$("#vrij").html("Request failed: " + textStatus);
			});
        }
    });
});

