$(function() {
    var dialog, form,
    email = $("#email"),
    password = $("#password"),
    allFields = $([]).add(email).add(password),
    tips = $(".errorTips");
 
    function updateTips(t) {
      tips
        .text(t)
        .addClass("ui-state-highlight");
      setTimeout(function() {
        tips.removeClass("ui-state-highlight", 1500);
      }, 500);
    }
      
    function checkEmpty(o, n) {
      if (o.val().length === 0) {
        o.addClass("ui-state-error");
        updateTips(n + " mag niet leeg zijn.");
        return false;
      } else {
        return true;
      }
    }
      
    function logIn() {
        var valid = true;
        valid = valid && checkEmpty(email, "E-mail");
        valid = valid && checkEmpty(password, "Wachtwoord");
        if (valid) {
            $('#btnlogin').click();   
        }    
    }
    dialog = $("#login-form").dialog({
      draggable: false,
      autoOpen: false,
      height: 400,
      width: 350,
      modal: true,
      buttons: {
        "Log in": logIn,
        Cancel: function() {
          dialog.dialog("close");
        }
      },
      close: function() {
        $("#frmlogin")[0].reset();
        allFields.removeClass("ui-state-error");
      }
    });
 
/* ORIGINEEL   
      form = dialog.find("form").on( "submit", function( event ) {
      event.preventDefault();
      logIn();
    });
*/
 
    $("#logIn").button().on("click", function() {
      dialog.dialog("open");
    });
  });