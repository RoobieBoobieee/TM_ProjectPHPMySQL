<!DOCTYPE html>
<html>
<head>    
<?php require('includes/header.php');?>
    
<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
 ?>  
    
<title><?php echo($title);?>Menu</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 

<?php
    
       
     if (isset($_GET["add"]) && isset($_GET["id"]))
        {
            $query =  "INSERT INTO `restaurant`.`tblrijsttafel` (`id`, `npeople`, `dishid`) VALUES (NULL, '" . mysqli_real_escape_string($conn,htmlspecialchars($_GET["id"])) . "', '" . mysqli_real_escape_string($conn,htmlspecialchars($_GET["add"])) . "')";
            mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        }
    
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM tblmenu WHERE '1';";
        
        // query uitvoeren
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
        // elke rij afgaan en afdrukken
        if ($result->num_rows > 0) {
            
          echo ("<table id=\"adminmenutable\" class=\"text ui-widget-content ui-corner-all fullwidth\">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class=\"fullwidth\">Gerecht</th>
                        <th>Prijs</th>
                        <th>Categorie</th>
                    </tr>
                </thead>
                <tbody>");
            
           // output data of each row
	       while($row = $result->fetch_assoc()) {
               
               $dish = new Dish();
               $dish->Nummer = $row["number"];
	           $dish->Naam =  $row["name"];
	           $dish->Prijs = $row["price"];
	           $dish->Beschrijving = $row["description"];
	           $dish->Categorie = $row["category"];
               
               echo ("<tr>" . $dish . "<td>" . $dish->Categorie . "</td><td>" . "<button class=\"btnaddrt\" id=\"" . mysqli_real_escape_string($conn,htmlspecialchars($_GET["id"])) . "&add=" . mysqli_real_escape_string($conn,htmlspecialchars($row["id"])) . "\">Toevoegen</button>" . "</td></tr>");
           }
            
           echo("</tbody></table>");
            
        }
?>
 
  </div>  
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>       