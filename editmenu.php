<!DOCTYPE html>
<html>
<head>
<?php require('includes/header.php');?>
<title><?php echo($title);?>Gerecht Aanpassen</title>
</head>
<body>

<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
?> 
    
<?php require('includes/nav.php');?>

<?php
    
    $number = "";
    $name = "";
    $price = "";
    $category = "";
    
    if (isset($_GET["id"])) {
        
        $query = "SELECT * FROM `tblmenu` WHERE `id` = '" . mysqli_real_escape_string($conn,htmlspecialchars($_GET["id"])) . "'";
        
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");

        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            
            $number = $row["number"];
            $name = $row["name"];
            $price = $row["price"];
            $category = $row["category"];
            
            $price = number_format((float)$price, 2, ',', '');
        }
    }
?>

    
<main class="singlecolumn"> 
     <form name="frmeditdish" action="posts/updatedish.php" onsubmit="return validateRegistration()" method="post">
        <fieldset>
            <legend>Gerecht</legend>
            <label for="number">Nummer:</label>
            <input type="text" name="number" id="number" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($number);?>" required>
            <label for="namedish">Gerecht:</label>
            <input type="text" name="namedish" id="namedish" class="ui-widget-content ui-corner-all" maxlength="64" value = "<?php echo($name);?>" required>
            <label for="price">Prijs:</label>
            <input type="text" name="price" id="price" class="ui-widget-content ui-corner-all" maxlength="64" value = "<?php echo($price);?>" required>
            <label for="category">Categorie:</label>
            <input type="text" name="category" id="category" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($category);?>" required>
        </fieldset>
    </form>
</main>
  
<?php require('includes/footer.php');?>
</body>  
</html>