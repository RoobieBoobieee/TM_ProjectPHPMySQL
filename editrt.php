<!DOCTYPE html>
<html>
<head>
    
<?php require('includes/header.php');?>

    
<title><?php echo($title);?>Rijsttafels</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu">  
        
    <?php
        if (isset($_GET["rem"]) && isset($_GET["id"]))
        {
            $query = "DELETE FROM `tblrijsttafel` WHERE `dishid` = " . mysqli_real_escape_string($conn,htmlspecialchars($_GET["rem"])) . " AND `npeople` = " . mysqli_real_escape_string($conn,htmlspecialchars($_GET["id"])) . ";";
            mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        }
            
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM `tblrijsttafel` WHERE `npeople` = " . mysqli_real_escape_string($conn,htmlspecialchars($_GET["id"])) . ";";
        
        // query uitvoeren
        $rt = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
    
    
        echo ("<table id=\"adminuserstable\" class=\"text ui-widget-content ui-corner-all fullwidth\"><tbody>");
    
        if ($rt->num_rows > 0) {
            while($rtrow = $rt->fetch_assoc()) {
               
               $query = "SELECT * FROM `tblmenu` WHERE `id` = " . mysqli_real_escape_string($conn,htmlspecialchars($rtrow["dishid"]));
               
               $menu = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
               
               if ($menu->num_rows > 0) {
	               while($menurow = $menu->fetch_assoc()) {
               
                    echo ("<tr>
                        <td>" . $menurow["name"] . "</td>
                        <td>" . "<button class=\"btnremdish\" id=\"id=" . $_GET["id"] . "&rem=" .$menurow["id"] . "\">Verwijderen</button>" . "</td></tr>");
                    }
               }
            }
        }
            
        echo("</tbody></table>");
        echo("<button class=\"btnaddrt\" id=\"" . $_GET["id"] . "\">Gerecht Toevoegen</button>");
            
 ?>       
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>