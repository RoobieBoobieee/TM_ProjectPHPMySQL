<!DOCTYPE html>
<html>
<head>
<?php require('includes/header.php');?>
<title><?php echo($title);?>Gebruiker Aanpassen</title>
</head>
<body>

<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
?> 
    
<?php require('includes/nav.php');?>
<?php
    
$name = "";
$lastname = "";
$email = "";
$street = "";
$number = "";
$postal = "";
$city = "";
$country = "";
$query = "SELECT * FROM `tblusers` WHERE `id` = '" . mysqli_real_escape_string($conn,htmlspecialchars($_GET['id'])) . "'";

$result = mysqli_query($conn, $query);

if ($result->num_rows > 0) {

$row = $result->fetch_assoc();
$name = $row["firstname"]; 
$lastname = $row["lastname"];
$email = $row["email"];
$street = $row["street"];
$number = $row["number"];
$postal = $row["postal"];
$city = $row["city"];
$country = $row["country"];
$level = $row["level"];
}
    
?>
          
<main class="doublecolumn"> 
    <div class="errorTips"></div>
     <form name="frmprofile" action="posts/updateprofile.php" onsubmit="return validateRegistration()" method="post">
         <input type="hidden" name="id" id="id" value = "<?php echo($_GET['id']);?>" required>
         
    <fieldset>
        <legend>Account</legend>
        <label for="name">Voornaam:</label>
        <input type="text" name="name" id="name" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($name);?>" required>
        <label for="lastname">Achternaam:</label>
        <input type="text" name="lastname" id="lastname" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($lastname);?>" required>
        <label for="email">E-mail:</label>
        <input type="text" name="email" id="email" class="ui-widget-content ui-corner-all" maxlength="64" value = "<?php echo($email);?>" required disabled="true">
        <label for="password">Wachtwoord:</label>
        <input type="password" value="" name="password" id="password" class="ui-widget-content ui-corner-all">
        <label for="REpassword">Herhaal wachtwoord:</label>
        <input type="password" value="" name="REpassword" id="REpassword" class="ui-widget-content ui-corner-all">
    </fieldset>
        
    <fieldset>
        <legend>Locatie</legend>
        <label for="street">Straat:</label>
        <input type="text" name="street" id="street" class="ui-widget-content ui-corner-all" maxlength="128" value = "<?php echo($street);?>" required>
        <label for="housenum">Nummer:</label>
        <input type="number" name="housenum" id="housenum" class="ui-widget-content ui-corner-all" maxlength="4" value = "<?php echo($number);?>" required>
        <label for="postal">Postcode:</label>
        <input type="postal" name="postal" id="postal" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($postal);?>" required>
        <label for="city">Stad:</label>
        <input type="city" name="city" id="city" class="ui-widget-content ui-corner-all" maxlength="8" value = "<?php echo($city);?>" required>
        <label for="country">Land:</label>
        <input type="text" name="country" id="country" class="ui-widget-content ui-corner-all" maxlength="32" value = "<?php echo($country);?>" required>
     </fieldset>
    
        <label for="admin">Administrator: </label>
        <input type="checkbox" name="admin" id="admin" <?php if ($level == 2){ echo("checked=\"true\"");}?>>
     <input type="submit" class="ui-widget-content ui-corner-all">
        
    </form>
</main>
  
<?php require('includes/footer.php');?>
</body>  
</html>