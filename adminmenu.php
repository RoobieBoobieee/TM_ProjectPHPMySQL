<!DOCTYPE html>
<html>
<head>    
<?php require('includes/header.php');?>
    
<?php
    if(!isAdmin()) {
        header('Location: index.php');
    }
 ?>  
    
<title><?php echo($title);?>Menu</title>
 
</head>
<body>
    
<?php require('includes/nav.php');?>

<main class="menu"> 

<?php
    
        // selecteer al de items in de menu tabel
        $query = "SELECT * FROM tblmenu WHERE '1';";
        
        // query uitvoeren
        $result = mysqli_query($conn, $query) or die("Rob heeft weer iets verkeerd gedaan.");
        
        // elke rij afgaan en afdrukken
        if ($result->num_rows > 0) {
            
          echo ("<table id=\"adminmenutable\" class=\"text ui-widget-content ui-corner-all fullwidth\">
                <thead>
                    <tr>
                        <th>#</th>
                        <th class=\"fullwidth\">Gerecht</th>
                        <th>Prijs</th>
                        <th>Categorie</th>
                    </tr>
                </thead>
                <tbody>");
            
           // output data of each row
	       while($row = $result->fetch_assoc()) {
               
               $dish = new Dish();
               $dish->Nummer = $row["number"];
	           $dish->Naam =  $row["name"];
	           $dish->Prijs = $row["price"];
	           $dish->Beschrijving = $row["description"];
	           $dish->Categorie = $row["category"];
               
               echo ("<tr>" . $dish . "<td>" . $dish->Categorie . "</td><td>" . "<button class=\"btneditmenu\" id=\"" . $row["id"] . "\">Aanpassen</button>" . "</td></tr>");
           }
            
           echo("</tbody></table>");
            
        }
?>
 
  </div>  
</main>

    
<?php require('includes/footer.php');?>
</body>  
</html>       