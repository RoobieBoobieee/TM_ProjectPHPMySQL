<?php

class Rijsttafel
{
    private $_soups = [];
    private $_entree;
    private $_dishes = [];
    private $_price;
    private $_npeople;
    
    public function __construct()
	{        
        $this->_soups = [];
        $this->_entree = "test";
        $this->_dishes = [];
        $this->_price = 0;
        $this->_npeople = 0;
	}  
    public function __toString()
    {
        $str = "";
        $str .= "<table class=\"text ui-widget-content menucategory center\" id=\"" . $this->_npeople . "\">";
        $str .= "<caption>&#21488; - Rijstafel " . $this->_npeople . " Personen <p class=\"right\">&euro;" . $this->_price . "p.p.</caption>";
        $str .= "<tbody><tr><td class=\"strong\">Soep naar keuze:</td></tr><tr><td>";
        $str .= $this->SoepPrinten();
        $str .= "<tr><td class=\"strong\">Voorgerecht:</td></tr><tr><td>";
        $str .= $this->_entree;
        $str .= "<tr><td class=\"strong\">Hoofdschotels:</td></tr><tr><td>";
        $str .= $this->GerechtPrinten();
        $str .= "</td>";
        return $str;
    }
    
    public function __set($property,$value)
	{
		switch($property)
		{		
			case "Voorgerecht":
			$this->_entree = $value;
			break;
			
			case "Prijs":
			$this->_price = number_format((float)$value, 2, ',', '');
			break;
			
			case "Personen":
			$this->_npeople = $value;
			break;	
                
		}
	}
    
     public function __get($property)
	{
		switch($property)
		{
            
            case "Voorgerecht":
			$result = $this->_entree;
			break;
			
			case "Prijs":
			$result = $this->_price;
			break;
			
			case "Personen":
			$result = $this->_npeople;
			break;
		
		}
        return $result;
	}
    
    
    public function SoepToevoegen($value)
    {
        foreach ($this->_soups as $soup) {
            if (strcmp($soup, $value) == 0)
            {
               exit (1);
            }
        }
        array_push($this->_soups, $value);
        
    }
    
    public function SoepPrinten()
    {
        if (isset($this->_soups))
        {
        sort($this->_soups);    
        $str = "";
        $length = count($this->_soups);
        $length -= 1;
        for($i=0; $i<$length; $i++)
        {
            $str .= $this->_soups[$i] . " of ";
        }
        $str .= $this->_soups[$length] . ".";
        return $str;
        }
    }
    
    public function GerechtToevoegen($value)
    {
        foreach ($this->_dishes as $dish) {
                if (strcmp($dish, $value) == 0)
                {
                    exit (1);
                }
            }
        array_push($this->_dishes, $value);
       
    }
    
    public function GerechtPrinten()
    {
        if (isset($this->_dishes))
        sort($this->_dishes);
        $str = "";   
        {
        $length = count($this->_dishes);
        $length -= 1;
        for($i=0; $i<$length; $i++)
        {
            $str .= ($i + 1) . ". " . $this->_dishes[$i] . "<br />";
        }
        $str .= ($length + 1) . ". " . $this->_dishes[$length] . ".";
        }
        return $str;
    }
	
}
?>