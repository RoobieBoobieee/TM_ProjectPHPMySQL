<?php

class Dish
{
    private $_number;
    private $_name;
    private $_price;
    private $_description;
    private $_category;
    
    public function __construct($number="", $name="", $price=0.0, $description="", $category="")
	{        
        $this->_number = $number;
        $this->_name = $name;
        $this->_price = $price;
        $this->_description = $description;
        $this->_category = $category;
	}
    
    public function __toString()
    {
        $str = "<td class=\"alignright\">" . $this->_number . "</td>";
        $str .= "<td>" . $this->_name;
        if(isset($this->_description)) 
        {
            $str .= "<p class=\"description\">" . $this->_description . "</p>";
        }
        $str .= "</td>";
        $str .= "<td class=\"alignright\">&euro;" . $this->_price ."</td>";
        return $str;
    }
    
    public function __set($property,$value)
	{
		switch($property)
		{
			case "Nummer":
			$this->_number = $value;
			break;
			
			case "Naam":
			$this->_name = $value;
			break;
			
			case "Prijs":
			$this->_price = number_format((float)$value, 2, ',', '');
			break;
			
			case "Beschrijving":
			$this->_description = $value;
			break;	
                
            case "Categorie":
			$this->_category = $value;
			break;	
		}
	}
    
     public function __get($property)
	{
		switch($property)
		{
			case "Nummer":
			$result = $this->_number;
			break;
			
			case "Naam":
			$result = $this->_name;
			break;
			
			case "Prijs":
			$result = $this->_price;
			break;
			
			case "Beschrijving":
			$result = $this->_description;
			break;	
                
			case "Categorie":
			$result = $this->_category;
			break;			
		}
        return $result;
	}
	
}
?>